from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def index(request):
    return render(request, "vista/administrador.html")

#def admi(request):
 #   return render(request, "vista/administrador.html")

def cliente(request):
    return render(request, "vista/cliente.html")
    
def pedidos(request):
    return render(request, "vista/pedidos.html")
 
def productos(request):
    return render(request, "vista/productos.html")

def facturacion(request):
    return render(request, "vista/factura.html")
