from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name ='inicio'),

   # path('admi', views.admi, name='admi'),
   
    path('cliente', views.cliente, name='cliente'),

    path('pedidos', views.pedidos, name='pedidos'),

    path('productos', views.productos, name='productos'),

    path('facturacion', views.facturacion, name='facturacion'),
]